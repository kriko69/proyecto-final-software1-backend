<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\bl\AmigoBl;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AmigoController extends Controller
{
    public function index(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);

        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl= new AmigoBl();
            return $bl->verMisAmigos($payload->sub);

        }

    }

    public function buscarAmigo(Request $request,$filtro,$valor)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);

        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {

            if (!isset($filtro))
            {
                $data=array(
                    'mensaje'=>'La variable filtro no existe'
                );
                return response()->json($data);
            }
            if (!isset($valor))
            {
                $data=array(
                    'mensaje'=>'La variable valor no existe'
                );
                return response()->json($data);
            }

            $id = $payload->sub;
            if (is_null($valor) && !is_null($filtro))
            {
                $data=array(
                    'mensaje'=>'Necesita poner un valor.'
                );
                return response()->json($data);
            }elseif (is_null($filtro)){
                /*$user=Usuario::where('usuario_id','<>',$id)
                    ->where('estado',false)
                    ->get();
                return response()->json($user);*/
            }else{
                $filtro=strtolower($filtro);
                $valor=strtolower($valor);
                $bl = new AmigoBl();
                return $bl->buscarAmigos($filtro,$valor,$id);

            }
        }
    }

    public function buscarAmigoTodos(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);

        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $id = $payload->sub;
            $bl = new AmigoBl();
            return $bl->buscarAmigosTodos($id);
        }
    }

}


