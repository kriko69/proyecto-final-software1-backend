<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Chat;
use Illuminate\Support\Facades\DB;
/**
* @OA\Info(title="API RedAlumni", version="1.0")
*
* @OA\Server(url="http://localhost:8000")
*/
class ChatController extends Controller
{
     /**
    * @OA\POST(
    *     path="/api/chats",
    *     summary="Crear chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
        public function crearchat(Request $request){

            $token=$request->header('Authorization',null);
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
        $nombre=$request->json("nombre");
        $descripcion=$request->json("descripcion");
        $tipo=$request->json("tipo");
        if(!is_null($nombre) && !is_null($descripcion) && !is_null($tipo)){
            $chat=new Chat();
            $chat->nombre=$nombre;
            $chat->descripcion=$descripcion;
            $chat->tipo=$tipo;
            DB::beginTransaction();
            try {
                $chat->save();
                $chat->usuarios()->attach($payload->sub);
                $data=array(
                    'mensaje'=>'chat creado con exito',
                    'descripcion'=>'exito',
                    'chat_id'=>$chat->chat_id
                );
                DB::commit();
            } catch (Exception $e) {
                $data=array(
                    'mensaje'=>'Error al realizar la transaccion',
                    'descripcion'=>'fallo'
                );
                DB::rollback();
            }

    }
    else{
        if(is_null($nombre)){
        $data=array(
            'mensaje'=>'chat no creado',
            'descripcion'=>'nombre es null'
        );}
        else if(is_null($descripcion)){
            $data=array(
                'mensaje'=>'chat no creado',
                'descripcion'=>'descripcion es null'
            );}
        else if(is_null($tipo)){
            $data=array(
                'mensaje'=>'chat no creado',
                'descripcion'=>'tipo es null'
            );}
        if(is_null($descripcion) && is_null($nombre) && is_null($tipo)){
            $data=array(
                'mensaje'=>'chat no creado',
                'descripcion'=>'descripcion es null'
            );}

    }

    return response()->json($data,200);
            }
    }
    /**
    * @OA\GET(
    *     path="/api/chats",
    *     summary="Listar un chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function listarchat(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $chat=DB::table('chat_usuario')
                ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
                ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
                ->select('chats.chat_id','chats.nombre','chats.descripcion','chats.tipo')
                ->where('chats.chat_id','=',$id)
                ->where('usuarios.usuario_id','=',$payload->sub)
                ->where('chats.estado','=',0)
                ->get();
            if (count($chat)!=0) {
                return response()->json($chat, 200);
            } else {
                $data = array(
                    'mensaje' => 'chat no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
    /**
    * @OA\PUT(
    *     path="/api/chats",
    *     summary="Actualizar un chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function actualizarchat(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $chat = Chat::where('chat_id','=',$id)->first();
            if(is_object($chat)){
            $nombre=$request->json("nombre");
            $descripcion=$request->json("descripcion");


            if ($nombre!=null) {
                $chat->nombre = $nombre;
            }
            if ($descripcion!=null) {
                $chat->descripcion = $descripcion;
            }
            if($nombre==null && $descripcion==null)
            {
                return response()->json(['error' => 'No hay nada para cambiar.', 'code' => 200]);
            }
            if (!$chat->isDirty()) {
                return response()->json(['error' => 'No hay nada cambiado.', 'code' => 200]);
            }
                        $chat->save();
                return response()->json(['exito' => 'Chat cambiado.','chat'=>$chat,'code'=>200]);
        }
            else{
                $data = array(
                    'mensaje' => 'chat no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
    /**
    * @OA\DELETE(
    *     path="/api/chats",
    *     summary="Borrar un chat",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function eliminarchat(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $chat = Chat::where('chat_id','=',$id)->first();
            if(is_object($chat)){
                $chat->estado=1;
                $chat->save();
                return response()->json(['exito' => 'Chat eliminado.','chat'=>$chat,'code'=>200]);
            }
            else{
                $data = array(
                    'mensaje' => 'chat no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
    public function listar_chats_usuario(Request $request){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $usuario_id = $payload->sub;
            $chat=DB::table('chat_usuario')
                ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
                ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
                ->select('chats.chat_id','chats.nombre','chats.descripcion','chats.tipo')
                ->where('usuarios.usuario_id','=',$payload->sub)
                ->where('chats.estado','=',0)
                ->get();
            if (count($chat)!=0) {
                return response()->json($chat, 200);
            } else {
                $data = array(
                    'mensaje' => 'chats no existen',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
    public function agregar_participante(Request $request,$id,$id2){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $chat = Chat::where('chat_id','=',$id)->first();
            if(is_object($chat)){
                $nombre=$request->json("nombre");
                $descripcion=$request->json("descripcion");


                if ($nombre!=null) {
                    $chat->nombre = $nombre;
                }
                if ($descripcion!=null) {
                    $chat->descripcion = $descripcion;
                }
                if($nombre==null && $descripcion==null)
                {
                    return response()->json(['error' => 'No hay nada para cambiar.', 'code' => 200]);
                }
                if (!$chat->isDirty()) {
                    return response()->json(['error' => 'No hay nada cambiado.', 'code' => 200]);
                }
                $chat->save();
                return response()->json(['exito' => 'Chat cambiado.','chat'=>$chat,'code'=>200]);
            }
            else{
                $data = array(
                    'mensaje' => 'chat no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
}
