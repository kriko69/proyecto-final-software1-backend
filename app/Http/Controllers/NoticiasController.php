<?php

namespace App\Http\Controllers;

use App\Models\Noticias;
use Dotenv\Validator;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class noticiasController extends Controller
{
    public function getNoticias(){

        $noticias=new Noticias();
        $dato=Noticias::all();
        return response()->json($dato,200);

    }

    /**
     * @param Request $request
     */
    public function publicacion(Request $request)
    {
     //   $token = $request->header("Authorization");
        $usuario_id = $request->json("usuario_id");
        $titulo = $request->json("titulo");
        $descripcion = $request->json("descripcion");
        $ubicacion = $request->json("ubicacion");
        $fecha = $request->json("fecha");
        $estado = $request->json("estado");

 //               return $token;
        if (!is_null($usuario_id) && !is_null($titulo) && !is_null($descripcion) && !is_null($ubicacion) && !is_null($fecha) && !is_null($estado)) {

            $noticia = new Noticia();
            $noticia->usuario_id = $usuario_id;
            $noticia->titulo = $titulo;
            $noticia->descripcion = $descripcion;
            $noticia->ubicacion = $ubicacion;
            $noticia->fecha = $fecha;
            $noticia->estado = true;
/*
            if ($noticia->save()) {
                return response()->json(['mensaje' => 'Se creo correctamente el registro']);
            } else {
                return response()->json(['mensaje' => 'Error al crear el registro']);
            }

//                $noticia->save();
//                echo("registro realizado");*/
            $isset_usuario=Usuario::where('usuario_id','=',$usuario_id)->first();

            if (is_object($isset_usuario))
            {

                DB::beginTransaction();
                try {
                    $noticia->save();
                    $data=array(
                        'mensaje'=>'El anuncio fue creado exitosamente.',
                        'descripcion'=>'exito.',
                        'id_usuario'=>$usuario_id
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                } catch (Throwable $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
            }else{
                $data=array(
                    'mensaje'=>'el usuario no es valido',
                    'descripcion'=>'El id del usuario no es valido'
                );
            }
        } else {
            $data = array(
                'mensaje' => 'noticia no creado',
                'descripcion' => 'algun parametro en null');
        }

       return response()->json($data,200);
    }
}
