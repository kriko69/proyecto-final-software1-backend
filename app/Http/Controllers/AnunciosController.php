<?php

namespace App\Http\Controllers;

// use App\Anuncios;
// use App\Models\Persona;
use App\Helpers\JwtAuth;
use App\Models\Anuncios;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnunciosController extends Controller
{
    public function registrarAnuncio(Request $request)
    {
        // Datos
        $token=$request->json('Authorization',null);
        $usuario_id=$request->json("usuario_id");
        $tipo=$request->json("tipo");
        $titulo=$request->json("titulo");
        $descripcion=$request->json("descripcion");
        $ubicacion=$request->json("ubicacion");
        $carrera=$request->json("carrera");
        $fecha_inicio=$request->json("fecha_inicio");   // AAAA/MM/DD
        $fecha_fin=$request->json("fecha_fin");
        $estado=$request->json("estado");

        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto o expirado.'
            );
            return response()->json($data);
        }else if(!is_null($usuario_id) && !is_null($tipo) && !is_null($titulo) && !is_null($descripcion) && !is_null($ubicacion) && !is_null($carrera) && !is_null($fecha_inicio)
            && !is_null($fecha_fin) && !is_null($estado)){

            $anuncio = new Anuncios();
            $anuncio->usuario_id = $usuario_id;
            $anuncio->tipo = $tipo;
            $anuncio->titulo = $titulo;
            $anuncio->descripcion = $descripcion;
            $anuncio->ubicacion = $ubicacion;
            $anuncio->carrera = $carrera;
            $anuncio->fecha_inicio = $fecha_inicio;
            $anuncio->fecha_fin = $fecha_fin;
            $anuncio->estado = $estado;

            $isset_usuario=Usuario::where('usuario_id','=',$usuario_id)->first();

            if (is_object($isset_usuario))
            {

                DB::beginTransaction();
                try {
                    $anuncio->save();
                    $data=array(
                        'mensaje'=>'El anuncio fue creado exitosamente.',
                        'descripcion'=>'exito.',
                        'id_usuario'=>$usuario_id
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                } catch (Throwable $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
            }else{
                $data=array(
                    'mensaje'=>'el usuario no es valido',
                    'descripcion'=>'El id del usuario no es valido'
                );
            }

        }else{

            $data=array(
                'mensaje'=>'El anuncio no pudo ser creado',
                'descripcion'=>'algun parametro tiene valor nulo (null)'
            );

        }

        return response()->json($data,200);

        /*$usuario=Usuario::find(2);
        return $usuario->persona->nombre;*/

    }

    public function listarAnuncios(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto o expirado.'
            );
            return response()->json($data);
        }else{
            // En caso de que el token sea valido, se lista todos los anuncios
            $anuncios = Anuncios::all()->where('estado','=',1);
            return response()->json($anuncios);

        }

    }

    /*public function eliminarAnuncios($id){
        $anuncio = Anuncios::findOrFail($id);
        $anuncio->delete();
        echo ("hola");
    }*/

    public function eliminarAnuncio(Request $request){
        $anuncio_id=$request->json("anuncio_id");
        $anuncios =Anuncios::findOrFail($anuncio_id);
        // $anuncios=Anuncios::where('anuncio_id','=',$anuncio_id)->first();
        $anuncios->estado = 0; // False
        $anuncios->save();
        return response()->json($anuncios,200);
    }

}
