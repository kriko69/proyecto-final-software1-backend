<?php

namespace App\Http\Controllers;

use App\Models\BolsaTrabajo;
use Dotenv\Validator;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BolsaTrabajoController extends Controller
{
    //lista todas las bolsas de trabajo
    public function getBolsaTrabajo()
    {
        $bolsaTrabajo=BolsaTrabajo::all();

        return response()->json($bolsaTrabajo);
        //return response($bolsaTrabajo,200)->header('Content-Type','text/xml');
    }

    //agrega una lista de trabajo
    public function postBolsaTrabajo(Request $request)
    {

        $validator=$this->validate($request,[
            "titulo" => "required|min:10",
            "descripcion" => "required|min:10",
            "ubicacion" => "required|min:5",
            "fecha_inicio" => "required",
            "fecha_fin" => "required",
            "carrera" => "required",

        ]);
        $titulo=$request->input("titulo");
        $descripcion=$request->input("descripcion");
        $ubicacion=$request->input("ubicacion");
        $fechaI=$request->input("fecha_inicio");
        $fechaF=$request->input("fecha_fin");
        $carrera=$request->input("carrera");
        $bolsaTrabajo= new BolsaTrabajo();
        $bolsaTrabajo->titulo=$titulo;
        $bolsaTrabajo->descripcion=$descripcion;
        $bolsaTrabajo->ubicacion=$ubicacion;
        $bolsaTrabajo->fecha_inicio=$fechaI;
        $bolsaTrabajo->fecha_fin=$fechaF;
        $bolsaTrabajo->carrera=$carrera;

        if($bolsaTrabajo->save())
        {
            return response()->json(['mensaje'=>'Se creo correctamente el registro']);
        }else{
            return response()->json(['mensaje'=>'Error al crear el registro']);
        }

    }

    //lista una en especifica
    public function getBolsa($id)
    {
        $bolsaTrabajo=BolsaTrabajo::where('id_bolsa_trabajo',$id)->get();
        if($bolsaTrabajo->isEmpty())
        {
            return response()->json(['mensaje'=>'El usuario no se encuentra en la base de datos.']);
        }else{
            return response()->json($bolsaTrabajo);
        }

    }

    //actualiza una bolsa
    public function putBolsaTrabajo($id)
    {
        return Response()->json(["mensaje"=> "hola"]);
    }

    //elimina una bolsa
    public function deleteBolsaTrabajo($id)
    {
        return Response()->json(["mensaje"=> "hola"]);
    }
}
