<?php

namespace App\Http\Controllers;

use App\Models\Privilegio;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\DB;

class PrivilegioController extends Controller
{
    public function getPrivilegios(Request $request){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if($payload){
            $usuario = Usuario::find($payload->sub);
            $roles = array();
            foreach ($usuario->roles as $rol){
                array_push($roles,$rol->rol_id);
            }
            if(in_array(1,$roles))
            {
                //es admin
                $priv = Privilegio::all()->where('estado','<>',true);
                return response()->json($priv);
            }else{
                $data=array(
                    'mensaje'=>'Solo acceso de Admin'
                );
                return response()->json($data);
            }
        }
        else{
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }      
    }
}
