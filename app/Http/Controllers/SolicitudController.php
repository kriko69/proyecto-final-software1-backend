<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Models\Solicitud;
use App\Models\Usuario;
use Illuminate\Http\Request;

class SolicitudController extends Controller
{
    function enviarSolicitud(Request $request)
    {
        $para=$request->json("para");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{
            if($payload->sub !=$para)
            {
                $isset_usuario=Usuario::where('usuario_id','=',$para)->first();
                if (!is_object($isset_usuario))
                {
                    $data=array(
                        'mensaje'=>'El usuario '.$para.' no existe.'
                    );
                    return response()->json($data);
                }else{
                    $solicitud = new Solicitud();
                    $solicitud->usuario_emisor_id=$payload->sub;
                    $solicitud->usuario_receptor_id=$para;
                    $solicitud->estado_solicitud='pendiente';
                    $solicitud->estado=false;
                    $solicitud->save();
                    $data=array(
                        'mensaje'=>'Solicitud enviada'
                    );
                    return response()->json($data);
                }

            }else{
                $data=array(
                    'mensaje'=>'No se puede enviar solicitud a usted.'
                );
                return response()->json($data);
            }

        }
    }

    function bandejaDeEntrada(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{
            $solicitudes = Solicitud::where('usuario_receptor_id','=',$payload->sub)->where('estado','=',false)->get();
            return response()->json($solicitudes);
        }
    }

    function bandejaDeSalida(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{
            $solicitudes = Solicitud::where('usuario_emisor_id','=',$payload->sub)->where('estado','=',false)->get();
            return response()->json($solicitudes);
        }
    }

    function verSolicitudEntrada(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{
            $solicitudes = Solicitud::where('usuario_receptor_id','=',$payload->sub)
                ->where('estado','=',false)
                ->where('solicitud_id','=',$id)
                ->get();
            return response()->json($solicitudes);
        }
    }

    function verSolicitudSalida(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{
            $solicitudes = Solicitud::where('usuario_emisor_id','=',$payload->sub)
                ->where('estado','=',false)
                ->where('solicitud_id','=',$id)
                ->get();
            return response()->json($solicitudes);
        }
    }

    function editarSolicitudEntrada(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{

        }
    }

    function editarSolicitudSalida(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{

        }
    }
    function eliminarSolicitudEntrada(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{

        }
    }

    function eliminarSolicitudSalida(Request $request,$id)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);

        }else{

        }
    }



}
