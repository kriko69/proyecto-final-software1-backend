<?php

namespace App\Http\Controllers\bl;



use App\Http\Controllers\dao\AmigoDao;

class AmigoBl
{
    function verMisAmigos($usuario_id)
    {
        $dao = new AmigoDao();
        $id_amigos=array();
        $user=$dao->obtenerUsuario($usuario_id);
        if(count($user->amigos)==0)
        {
            $data=array(
                'mensaje'=>'Usted no tiene amigos.'
            );
            return response()->json($data);
        }else{
            foreach ($user->amigos as $amigo)
            {
                array_push($id_amigos,$amigo->usuario_amigo_id);
            }
            return response()->json($dao->listarMisAmigosPorId($id_amigos));
        }
    }
    function buscarAmigos($filtro, $valor,$id)
    {
        $dao = new AmigoDao();
        switch ($filtro) {
            case 'nombre':
                return $dao->filtrarPorNombre($valor,$id);
                break;
            case 'carrera':
                return $dao->filtrarPorCarrera($valor,$id);
                break;
            case 'ci':
                $valor = (int)$valor;
                if ($valor == 0) {
                    $data = array(
                        'mensaje' => 'El valor debe ser un entero positivo.'
                    );
                    return response()->json($data);
                } else {

                   return $dao->filtrarPorCi($valor,$id);
                }

                break;
            default:
                $data = array(
                    'mensaje' => 'Filtro incorrecto.'
                );
                return response()->json($data);
        }
    }

    function buscarAmigosTodos($id)
    {
        $dao = new AmigoDao();
        return $dao->listarAmigosTodos($id);
    }
}
