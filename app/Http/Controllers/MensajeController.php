<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Models\Mensaje;
use Illuminate\Support\Facades\DB;
class MensajeController extends Controller
{
     /**
    * @OA\POST(
    *     path="/api/mensajes",
    *     summary="Crear mensaje",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function crearmensaje(Request $request,$chat_id){

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $usuario_id=$payload->sub;
            $mensaje1=$request->json("mensaje");
            if(!is_null($mensaje1)){
                $mensaje=new Mensaje();
                $mensaje->usuario_id=$usuario_id;
                $mensaje->chat_id=$chat_id;
                $mensaje->mensaje=$mensaje1;
                DB::beginTransaction();
                try {
                    $mensaje->save();$data=array(
                        'mensaje'=>'mensaje creado con exito',
                        'descripcion'=>'exito',
                        'chat_id'=>$mensaje->mensaje_id
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }

            }
            else{
                if(is_null($mensaje1)){
                    $data=array(
                        'mensaje'=>'chat no creado',
                        'descripcion'=>'mensaje es null'
                    );}
            }

            return response()->json($data,200);
        }
    }
    /**
    * @OA\GET(
    *     path="/api/mensajes/{$id}",
    *     summary="Listar un mensaje",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function listarmensaje(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $mensaje=DB::table('chat_usuario')
                ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
                ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
                ->join('mensajes','mensajes.chat_id','=','chats.chat_id')
                ->select('mensajes.mensaje_id','mensajes.chat_id','mensajes.usuario_id','mensajes.mensaje')
                ->where('mensajes.mensaje_id','=',$id)
                ->where('usuarios.usuario_id','=',$payload->sub)
                ->where('mensajes.estado','=',0)
                ->get();
            if (count($mensaje)!=0) {
                return response()->json($mensaje, 200);
            } else {
                $data = array(
                    'mensaje' => 'mensaje no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
    /**
    * @OA\DELETE(
    *     path="/api/mensajes/{$id}",
    *     summary="Borrar un mensaje",
    *     @OA\Response(
    *         response=200,
    *         description="Exito."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Error."
    *     )
    * )
    */
    public function eliminarmensaje(Request $request,$id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $mensaje = Mensaje::where('mensaje_id','=',$id)->first();
            if(is_object($mensaje)){
                $mensaje->estado=1;
                $mensaje->save();
                return response()->json(['exito' => 'Mensaje eliminado.','chat'=>$mensaje,'code'=>200]);
            }
            else{
                $data = array(
                    'mensaje' => 'Mensaje no existe',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
    public function listar_mensajes_chat(Request $request,$chat_id){
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else {
            $usuario_id = $payload->sub;
            $mensaje=DB::table('chat_usuario')
                ->join('usuarios','usuarios.usuario_id','=','chat_usuario.usuario_id')
                ->join('chats','chats.chat_id','=','chat_usuario.chat_id')
                ->join('mensajes','mensajes.chat_id','=','chats.chat_id')
                ->select('mensajes.mensaje_id','mensajes.chat_id','mensajes.usuario_id','mensajes.mensaje')
                ->where('chats.chat_id','=',$chat_id)
                ->where('usuarios.usuario_id','=',$payload->sub)
                ->where('mensajes.estado','=',0)
                ->get();
            if (count($mensaje)!=0) {
                return response()->json($mensaje, 200);
            } else {
                $data = array(
                    'mensaje' => 'mensajes no existen',
                    'descripcion' => 'descripcion es null'
                );
                return response()->json($data, 200);
            }
        }
    }
}
