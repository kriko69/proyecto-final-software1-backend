<?php

namespace App\Http\Controllers;

use App\Models\Foro_Pregunta;
use App\Models\Usuario;
use Dotenv\Validator;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\JwtAuth;

class Foro_PreguntaController extends Controller
{
    public function postForo_Pregunta(Request $request){  
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if($payload){
            $pregunta=$request->json("pregunta");
            $id_usuario = $payload->sub;
            //$id = Usuario::find($id_usuario); te devuelve todo como select
            if(!is_null($pregunta)){
                $foro = new Foro_Pregunta();
                $foro->usuario_id=$id_usuario;
                $foro->pregunta=$pregunta;
                $foro->estado=false;
                //return response()->json($foro);
                DB::beginTransaction();
                try {
                    $foro->save();
                    $data=array(
                        'mensaje'=>'Pregunta creada con exito',
                        'descripcion'=>'exito'
                    );
                    DB::commit();
                } catch (Exception $e) {
                    $data=array(
                        'mensaje'=>'Error al realizar la transaccion',
                        'descripcion'=>'fallo'
                    );
                    DB::rollback();
                }
            }
            else{
                $data=array(
                    'mensaje'=>'Pregunta no creada',
                    'descripcion'=>'No hizo una pregunta'
                );
            }
            return response()->json($data);
        }
        else{
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }      
    
    }
    public function getForo_Pregunta(Request $request){
        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $foro = Foro_Pregunta::all()->where('estado','<>',true);
            return response()->json($foro);
        }
    }
    
    public function getForo_Pregunta_especifica(Request $request,$id){
        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id= (int) $id;
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $foro = Foro_Pregunta::all()->where('foro_pregunta_id','=',$id)->where('estado','=',false);
            if(json_decode($foro, true)){
                return response()->json($foro);
            }
            else {
                $data= array(
                    'mensaje'=>'No existe la pregunta que busca'
                );
                return response()->json($data);
            }
        }
    }
    
    public function actualizarPregunta(Request $request,$id){
        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id= (int) $id;
        $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $pregunta=$request->json("pregunta");
                $usuario_id = $payload->sub;
                if(!is_null($pregunta)){
                    $foro=Foro_Pregunta::find($usuario_id);
                    if($foro->usuario_id == $id){
                        $foro->usuario_id=$usuario_id;
                        $foro->pregunta=$pregunta;
                        $foro->estado=false;
                        DB::beginTransaction();
                        try {
                            $foro->save();
                            $data=array(
                                'mensaje'=>'Pregunta actualizada con exito',
                                'descripcion'=>'exito'
                            );
                            DB::commit();
                        } catch (Exception $e) {
                            $data=array(
                                'mensaje'=>'Error al realizar la transaccion',
                                'descripcion'=>'fallo'
                            );
                            DB::rollback();
                        }
                    }
                    else{
                        $data=array(
                            'mensaje'=>'El usuario no tiene acceso a la pregunta'
                        );

                    }
                
                }
                else{
                    $data=array(
                        'mensaje'=>'Pregunta no cambiada',
                        'descripcion'=>'No hizo una pregunta'
                    );
                }
                return response()->json($data);
            }
    }
    
    public function eliminarPregunta(Request $request,$id){
        
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $id= (int) $id;
        $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $usuario_id = $payload->sub;
                if(!is_null($usuario_id)){
                    $foro=Foro_Pregunta::find($usuario_id);
                    if($foro->usuario_id == $id){
                        $foro->usuario_id=$usuario_id;
                        $foro->estado=true;
                        DB::beginTransaction();
                        try {
                            $foro->save();
                            $data=array(
                                'mensaje'=>'Pregunta eliminada con exito',
                                'descripcion'=>'exito'
                            );
                            DB::commit();
                        } catch (Exception $e) {
                            $data=array(
                                'mensaje'=>'Error al realizar la transaccion',
                                'descripcion'=>'fallo'
                            );
                            DB::rollback();
                        }
                    }
                    else{
                        $data=array(
                            'mensaje'=>'El usuario no tiene acceso a la pregunta'
                        );

                    }
                
                }
                else{
                    $data=array(
                        'mensaje'=>'Pregunta no se pudo eliminar',
                        'descripcion'=>'No hizo una pregunta'
                    );
                }
                return response()->json($data);
            }
    }
}
