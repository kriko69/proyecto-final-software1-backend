<?php

namespace App\Http\Controllers\dao;



use App\Models\Usuario;
use Illuminate\Support\Facades\DB;

class AmigoDao
{
    function obtenerUsuario($usuario_id)
    {
        return Usuario::find($usuario_id);
    }
    function listarMisAmigosPorId($id_amigos)
    {
        return DB::table('usuarios')
            ->join('persona','persona.persona_id','=','usuarios.persona_id')
            ->select('persona.nombre','persona.apellidos','persona.fecha_nacimiento',
                'persona.sexo','persona.estado_civil','persona.hijos','usuarios.ci','usuarios.carrera')
            ->whereIn('usuarios.usuario_id',$id_amigos)
            ->get();
    }

    function filtrarPorNombre($valor,$id)
    {
        $user = DB::table('usuarios')
            ->join('persona', 'persona.persona_id', '=', 'usuarios.persona_id')
            ->select('persona.nombre', 'persona.apellidos', 'persona.fecha_nacimiento',
                'persona.sexo', 'persona.estado_civil', 'persona.hijos', 'usuarios.ci', 'usuarios.carrera')
            ->where('usuarios.usuario_id', '<>', $id)
            ->where('usuarios.estado', '=', false)
            ->where('persona.nombre', 'like', '%' . $valor . '%')
            ->OrWhere('persona.apellidos', 'like', '%' . $valor . '%')
            ->get();

        if (count($user) == 0) {
            $data = array(
                'mensaje' => 'No se encontro amigos.'
            );
            return response()->json($data);
        } else {
            return response()->json($user);
        }
    }

    function filtrarPorCarrera($valor,$id)
    {
        $user = DB::table('usuarios')
            ->join('persona', 'persona.persona_id', '=', 'usuarios.persona_id')
            ->select('persona.nombre', 'persona.apellidos', 'persona.fecha_nacimiento',
                'persona.sexo', 'persona.estado_civil', 'persona.hijos', 'usuarios.ci', 'usuarios.carrera')
            ->where('usuarios.usuario_id', '<>', $id)
            ->where('usuarios.estado', '=', false)
            ->where('usuarios.carrera', 'like', '%' . $valor . '%')
            ->get();

        if (count($user) == 0) {
            $data = array(
                'mensaje' => 'No se encontro amigos.'
            );
            return response()->json($data);
        } else {
            return response()->json($user);
        }
    }

    function filtrarPorCi($valor,$id)
    {
        $user = DB::table('usuarios')
            ->join('persona', 'persona.persona_id', '=', 'usuarios.persona_id')
            ->select('persona.nombre', 'persona.apellidos', 'persona.fecha_nacimiento',
                'persona.sexo', 'persona.estado_civil', 'persona.hijos', 'usuarios.ci', 'usuarios.carrera')
            ->where('usuarios.usuario_id', '<>', $id)
            ->where('usuarios.estado', '=', false)
            ->where('usuarios.ci', $valor)
            ->get();
        if (count($user) == 0) {
            $data = array(
                'mensaje' => 'No se encontro amigos.'
            );
            return response()->json($data);
        } else {
            return response()->json($user);
        }
    }

    function listarAmigosTodos($id)
    {
        $user = DB::table('usuarios')
            ->join('persona', 'persona.persona_id', '=', 'usuarios.persona_id')
            /*->join("amigos","amigos.amigos_id","<>",'usuarios.usuario_id')*/
            ->select('persona.nombre', 'persona.apellidos', 'persona.fecha_nacimiento',
                'persona.sexo', 'persona.estado_civil', 'persona.hijos', 'usuarios.ci', 'usuarios.carrera')
            ->where('usuarios.usuario_id', '<>', $id)
            ->where('usuarios.estado', '=', false)
            ->get();
        if (count($user) == 0) {
            $data = array(
                'mensaje' => 'No se encontro amigos.'
            );
            return response()->json($data);
        } else {
            return response()->json($user);
        }
    }
}
