<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Privilegio extends Model
{

    protected $table='privilegios';
    protected $primaryKey='privilegio_id';
    protected $fillable = [
        'tipo','estado'];
/*

    public function rol_privilegio(){
        return $this->belogstoMany(Rol_Privilegio::class);
    }
*/
}
