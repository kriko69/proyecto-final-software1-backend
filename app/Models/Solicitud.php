<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = 'solicitudes';
    protected $primaryKey='solicitud_id';
    protected $fillable = [
        'usuario_emisor_id','usuario_receptor_id','estado_solicitud','estado'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }
}
