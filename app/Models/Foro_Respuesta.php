<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foro_Respuesta extends Model
{
    protected $table='foro_respuestas';
    protected $primaryKey='foro_respuesta_id';
    protected $fillable = [
        'usuario_id','foro_pregunta_id','pregunta','fecha','estado'];

    public function usuarios()
    {
        return $this->hasMany(Usuario::class);
    }

    public function foro_preguntas()
    {
        return $this->hasMany(Foro_Pregunta::class);
    }
}
