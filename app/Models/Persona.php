<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{

    protected $table='persona';
    protected $primaryKey='persona_id';
    protected $fillable = [
        'nombre','apellidos','fecha_nacimiento',
        'estado_civil','hijos','estado',
    ];

    public function usuarios()
    {
        return $this->hasMany(Usuario::class);
    }
}
