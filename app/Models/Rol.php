<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table='roles';
    protected $primaryKey='rol_id';
    protected $fillable = ['rol_id',
        'tipo','estado'];
/*

    public function usuarios(){
        return $this->belogstoMany(Usuario::class);
    }
*/

    public function usuarios(){
        return $this->belongsToMany(Usuario::class,'rol_usuario','rol_id','usuario_id');
    }
}
