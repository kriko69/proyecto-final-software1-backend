<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class noticias extends Model
{
    protected $table='noticias';
    protected $primaryKey='noticias_id';
    protected $fillable = [
        'titulo',
        'descripcion',
        'ubicacion',
        'fecha',
        'estado',
    ];

    /*public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }*/
}
