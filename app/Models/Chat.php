<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';
    protected $primaryKey='chat_id';
    const borrado=true;
    const NOborrado=false;
    const tipoGrupo='Grupal';
    const tipoIndividual='Individual';
    protected $fillable=[
        'chat_id',
        'nombre',
        'descripcion',
        'estado',
        'tipo',
    ];
    public function usuarios(){
        return $this->belongsToMany(Usuario::class,'chat_usuario','chat_id','usuario_id');
    }
}
