<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    protected $table = 'mensajes';
    protected $primaryKey='mensaje_id';
    const borrado=true;
    const NOborrado=false;
    protected $fillable=[
        'mensaje_id',
        'usuario_id',
        'chat_id',
        'mensaje',
        'fecha',
        'estado',
    ];
    public function estaBorrado(){
        return $this->borrar==Chat::borrado;
    }
    public function chats(){
        return $this->belongsToMany(Chat::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
