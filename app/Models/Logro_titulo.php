<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logro_titulo extends Model
{
    protected $table='logro_titulo';
    protected $primaryKey='titulo_id';
    protected $fillable = [
        'tipo',
        'nombre',
        'descripcion',
        'lugar',
        'pais',
        'fecha',
        'estado',
    ];

    /*public function usuario()
    {
        return $this->belongsTo(Usuario::class);
    }*/
}
