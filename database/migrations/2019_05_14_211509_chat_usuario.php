<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChatUsuario extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('chat_usuario', function (Blueprint $table) {
                $table->increments('chat_usuario_id');
                $table->integer('usuario_id')->unsigned();
                $table->integer('chat_id')->unsigned();
                $table->foreign('usuario_id')->references('usuario_id')->on('usuarios');
                $table->foreign('chat_id')->references('chat_id')->on('chats');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('chat_usuario');
        }
    }
