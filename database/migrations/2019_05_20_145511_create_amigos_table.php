<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmigosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigos', function (Blueprint $table) {
            $table->increments('amigos_id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('usuario_amigo_id')->unsigned();
            $table->boolean('estado');
            $table->timestamps();
            $table->foreign('usuario_id')->references('usuario_id')->on('usuarios');
            $table->foreign('usuario_amigo_id')->references('usuario_id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigos');
    }
}
