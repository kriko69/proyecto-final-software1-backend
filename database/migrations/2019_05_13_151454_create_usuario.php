<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('usuario_id');
            $table->string('carrera');
            $table->integer('ci')->unsigned();
            $table->integer('año_egreso')->unsigned();
            $table->string('email');
            $table->integer('celular')->unsigned();
            $table->integer('telefono')->unsigned();
            $table->string('ciudad');
            $table->string('password');
            $table->boolean('estado');
            $table->integer('persona_id')->unsigned();
            $table->timestamps();

            $table->foreign('persona_id')->references('persona_id')->on('persona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
