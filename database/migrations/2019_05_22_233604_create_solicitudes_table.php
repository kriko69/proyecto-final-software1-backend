<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('solicitud_id');
            $table->integer('usuario_emisor_id')->unsigned();
            $table->integer('usuario_receptor_id')->unsigned();
            $table->string('estado_solicitud');
            $table->boolean('estado');
            $table->timestamps();
            $table->foreign('usuario_emisor_id')->references('usuario_id')->on('usuarios');
            $table->foreign('usuario_receptor_id')->references('usuario_id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
