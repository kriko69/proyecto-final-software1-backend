<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogroTitulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logro_titulo', function (Blueprint $table) {
            $table->increments('titulo_id');
            $table->integer('usuario_id')->unsigned();
            $table->string('tipo');
            $table->string('nombre');
            $table->string('descripcion');
            $table->string('lugar');
            $table->string('pais');
            $table->date('fecha');
            $table->boolean('estado');
            $table->foreign('usuario_id')->references('usuario_id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logro_titulo');
    }
}
