<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function (Blueprint $table) {
            $table->increments('mensaje_id');
            $table->integer('usuario_id')->unsigned();;
            $table->integer('chat_id')->unsigned();;
            $table->string('mensaje');
            $table->boolean('estado')->default(false);
            $table->foreign('usuario_id')->references('usuario_id')->on('usuarios');
            $table->foreign('chat_id')->references('chat_id')->on('chats');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensajes');
    }
}
